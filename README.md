# npi-registry-api #

# Installation / setup #

## node ##
```
 npm install npi-registry-api --save
```

## usage ##

* use the various methods to setup the search criteria and then call _search()_ to perform the main operation.
* _search()_ returns a [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise).
```
const {NpiRegistry} = require('npi-registry-api')

new NpiRegistry()
//.firstName('john')
.lastName('smith')
.city('johnstown')
.state('PA')
//.number(<10-digit-number>)
.postalCode('159054305')
.countryCode('US')
.limit(1)
.skip(0)
.search()
.then((result)=>{
    console.log(`${JSON.stringify(result)}`)
})
.catch((err)=>{
    console.log(`PROBLEM, err=${err}`)
})
```
## output ##
```
{
  "result_count": 1,
  "results": [
    {
      "taxonomies": [
        {
          "state": "PA",
          "code": "363AM0700X",
          "primary": true,
          "license": "MA002741L",
          "desc": "Physician Assistant Medical"
        }
      ],
      "addresses": [
        {
          "city": "JOHNSTOWN",
          "address_2": "SUITE 130",
          "telephone_number": "814-534-5724",
          "fax_number": "814-536-1786",
          "state": "PA",
          "postal_code": "159054330",
          "address_1": "1111 FRANKLIN ST",
          "country_code": "US",
          "country_name": "United States",
          "address_type": "DOM",
          "address_purpose": "LOCATION"
        },
        {
          "city": "JOHNSTOWN",
          "address_2": "",
          "telephone_number": "814-410-8300",
          "fax_number": "814-410-8331",
          "state": "PA",
          "postal_code": "159054305",
          "address_1": "1086 FRANKLIN ST",
          "country_code": "US",
          "country_name": "United States",
          "address_type": "DOM",
          "address_purpose": "MAILING"
        }
      ],
      "created_epoch": 1137974400,
      "identifiers": [],
      "other_names": [],
      "number": 1952381162,
      "last_updated_epoch": 1406160000,
      "basic": {
        "status": "A",
        "credential": "PAC",
        "first_name": "TODD",
        "last_name": "SMITH",
        "middle_name": "E",
        "name": "SMITH TODD",
        "gender": "M",
        "sole_proprietor": "NO",
        "last_updated": "2014-07-24",
        "enumeration_date": "2006-01-23"
      },
      "enumeration_type": "NPI-1"
    }
  ]
}
```

# Links #

* [hhs demo](https://npiregistry.cms.hhs.gov/api/demo)
* [npi validator algorithm](http://makdns.blogspot.com/2012/07/national-provider-identifier-npi.html)