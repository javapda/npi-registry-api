const axios = require('axios')
const _ = require('lodash')
const NpiValidator = require('./npi-validator')

module.exports = class NpiRegistry {
    constructor() {
        this.serverUrl='https://npiregistry.cms.hhs.gov/api/'
        this.params = {
            number:'',
            enumeration_type:'',
            taxonomy_description:'',
            first_name:'',
            sidney:'',
            last_name:'',
            organization_name:'',
            address_purpose:'',
            city:'',
            state:'',
            postal_code:'',
            country_code:'',
            limit:'',
            skip:''
        }
    }
    city(city) {
        this._city = city
        return this
    }
    state(state) {
        this._state = state
        return this
    }
    firstName(firstName) {
        this._firstName = firstName
        return this
    }
    lastName(lastName) {
        this._lastName = lastName
        return this
    }
    number(number) {
        this._number = number
        return this
    }
    countryCode(countryCode) {
        this._countryCode = countryCode
        return this
    }
    postalCode(postalCode) {
        this._postalCode = postalCode
        return this
    }
    limit(limit) {
        this._limit = limit
        return this
    }
    skip(skip) {
        this._skip = skip
        return this
    }
    addressPurpose(addressPurpose) {
        this._addressPurpose = addressPurpose
        return this
    }
    search() {
        return new Promise((resolve,reject)=>{
            const url = this.serverUrl
            const params = _.clone(this.params)
            //console.log(this.serverUrl)
            if (this._countryCode) params.country_code = this._countryCode
            if (this._city) params.city = this._city
            if (this._firstName) params.first_name = this._firstName
            if (this._lastName) params.last_name = this._lastName
            if (this._limit) params.limit = this._limit
            if (this._state) params.state = this._state
            if (this._number) params.number = this._number
            if (this._addressPurpose) params.address_purpose = this._addressPurpose
            if (this._postalCode) params.postal_code = this._postalCode
            if (this._skip) params.skip = this._skip
            //props.headers = {"Accept":"*/*"}
            const props = {params:params}
            //console.log(`props = ${JSON.stringify(props)}`)
            // do call
            axios.get(url,props)
            .then((result)=>{
                resolve(result.data)
            })
            .catch((err)=>reject(err))
        })
    }

    static isValidNpi(npi){
        return new NpiValidator(npi).isValid() //true //`could be valid npi ${npi}`;
    }
}
