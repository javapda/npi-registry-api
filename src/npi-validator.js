// http://makdns.blogspot.com/2012/07/national-provider-identifier-npi.html
module.exports = class NpiValidator {
    constructor(npi) {
        this.npi = npi
    }
    isValid() {
        const npi = this.npi
        const last = npi.charAt(9)
        const first8 = npi.substring(0,9)
        const prefixedFirst8 = '80840'+first8
        const nextHigherNumberEndingInZero=(num)=> {
            num++
            while (num%10!=0) {
                num++
            }
            return num
        }
        const sumOfDigits=(num)=>{
            const numText=""+num
            let sum=0
            for (var i=0;i<numText.length;i++) {
                sum+=parseInt(numText.charAt(i))
            }
            return sum
        }
        const proceedWith = (num) => {
            const dbls = []
            const unaffected = []
            for (var i=num.length-1;i>=0;i--) {
                if (i%2==1) {
                    const number = parseInt(num.charAt(i))
                    dbls.push(number*2)
                } else {
                    unaffected.push(parseInt(num.charAt(i)))
                }
            }
            const nextHigherNumberEndingInZero=(num)=> {
                num++
                while (num%10!=0) {
                    num++
                }
                return num
            }
            const ns=dbls.map((num)=>sumOfDigits(num)).reduce((a,b)=>a+b, 0)
            +unaffected.map((num)=>sumOfDigits(num)).reduce((a,b)=>a+b, 0)
            const ns0=nextHigherNumberEndingInZero(ns)
            const diff=ns0-ns
            if (last==diff) {
                console.log(`yes ${npi} is valid`)
            } else {
                console.log(`no ${npi} is not valid`)
            }
            return (last==diff)
        }        
        return proceedWith(prefixedFirst8)
    }
}


// console.log('npi-validator')


// const npi = '1558598797'//'1740243039'// carusi

// console.log('=========================')
// new NpiValidator(npi).isValid()
// new NpiValidator('1740243039').isValid()