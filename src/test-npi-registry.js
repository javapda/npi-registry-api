const NpiRegistry = require('./npi-registry')

const testNpiRegistry=() => {
new NpiRegistry()
// .firstName('sidney')
// .lastName('goldblatt')
// .city('johnstown')
// .state('PA')
// .number(1801856224)
// .postalCode('159054305')
// .countryCode('US')
// .limit(1)
// .skip(0)
// .firstName('john')
.lastName('smith')
.city('johnstown')
.state('PA')
//.number(<10-digit-number>)
.postalCode('159054305')
.countryCode('US')
.limit(1)
.skip(0)

.search()
.then((result)=>{
    console.log(`result=${JSON.stringify(result)}`)
})
.catch((err)=>{
    console.log(`PROBLEM, err=${err}`)
})
}

const testNpiIsValid = () => {
    const npis = [
        '1801856224',// Dr. G
        '1740243039',// carusi
        '1740243034', // not valid
        '1356585673'  // yes valid
    ]
    for (var i=0; i < npis.length; i++) {
        const npi=npis[i]
        console.log(`${npi} - ${NpiRegistry.isValidNpi(npi)}`)
    }
}
testNpiIsValid()
// testNpiRegistry()
